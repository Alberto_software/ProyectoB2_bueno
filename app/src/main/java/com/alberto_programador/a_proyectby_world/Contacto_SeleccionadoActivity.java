package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Contacto_SeleccionadoActivity extends AppCompatActivity {
    ImageButton bREton;
    ImageButton bBorr;
    TextView Phone;
    TextView Contac;
    TextView Email;
    Button bModif;
    int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto__seleccionado);

        bREton=(ImageButton) findViewById(R.id.reton);
        bBorr=(ImageButton) findViewById(R.id.delet);

        Contac=(TextView) findViewById(R.id.nomUsu);
        Email= (TextView) findViewById(R.id.E_mail);
        Phone= (TextView) findViewById(R.id.mos_Tel);

        bModif= (Button) findViewById(R.id.bModi2);

        Bundle parametros = getIntent().getExtras();
        pos=(Integer) parametros.get("pos");
        Contac.setText(Contactos.Usus.get(pos).toString());
        Email.setText(Contactos.Correos.get(pos).toString());
        Phone.setText(Contactos.phones.get(pos).toString());

        bREton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg= new Intent(Contacto_SeleccionadoActivity.this,Area_ContactosActivity.class);
                startActivity(reg);
                finish();
            }
        });

        bBorr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contactos.borrar(1,pos);
                Contactos.borrar(2,pos);
                Contactos.borrar(3,pos);
                Intent reg2= new Intent(Contacto_SeleccionadoActivity.this,Area_ContactosActivity.class);
                startActivity(reg2);
                finish();
            }
        });

        bModif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent modif= new Intent(Contacto_SeleccionadoActivity.this,Nuevo_ContactosActivity.class);
                modif.putExtra("Validador",true);
                modif.putExtra("pos",pos);
                startActivity(modif);
                finish();
            }
        });
    }
}
