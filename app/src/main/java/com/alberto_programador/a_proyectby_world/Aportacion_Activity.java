package com.alberto_programador.a_proyectby_world;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class Aportacion_Activity extends AppCompatActivity {

    private static final String Config_Environment= PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String Config_Client_ID= "AammOzgjuwac_SIQ7r4oJ1fzZa0CoEHBIujYdUTElII3YaOICX_u26FJ2utYkZHanirgo2s-vhdO1NLI";
    private static final int Request_Code_Payment=1;
    private static PayPalConfiguration config= new PayPalConfiguration()
            .environment(Config_Environment)
            .clientId(Config_Client_ID)

            .merchantName("Mi tienda")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.mi_tienda.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.mi_tienda.com/legal"));
    PayPalPayment donar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aportacion_);
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
        findViewById(R.id.btton_Aport).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                donar = new PayPalPayment(new BigDecimal("100"), "USD",
                        "Proyecto X", PayPalPayment.PAYMENT_INTENT_SALE);
                Intent intent = new Intent(Aportacion_Activity.this,
                        PaymentActivity.class);

                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, donar);

                startActivityForResult(intent, Request_Code_Payment);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data
                    .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {

                    // informacion extra del pedido
                    System.out.println(confirm.toJSONObject().toString(4));
                    System.out.println(confirm.getPayment().toJSONObject()
                            .toString(4));

                    Toast.makeText(getApplicationContext(), "Orden procesada",
                            Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            System.out.println("El usuario canceló el pago");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
