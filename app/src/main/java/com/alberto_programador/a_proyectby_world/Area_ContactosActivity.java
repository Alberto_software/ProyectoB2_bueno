package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class Area_ContactosActivity extends AppCompatActivity {

    private ImageButton butAgre,butRegre;
    private ListView list_usu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area__contactos);

        butAgre = (ImageButton) findViewById(R.id.imageButton);
        butRegre = (ImageButton) findViewById(R.id.return1);

        list_usu = (ListView) findViewById(R.id.list_item_usu);

        butRegre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regre= new Intent(Area_ContactosActivity.this,MainActivity.class);
                startActivity(regre);
                finish();

            }
        });

        ArrayAdapter<String> Recorre_list_usu= new ArrayAdapter<String>
                (this,android.R.layout.simple_expandable_list_item_1,Contactos.Usus);
        list_usu.setAdapter(Recorre_list_usu);

        butAgre.setOnClickListener( new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent pasVent= new Intent(Area_ContactosActivity.this,Nuevo_ContactosActivity.class);
                pasVent.putExtra("Validador",false);
                startActivity(pasVent);
                finish();
            }
        });

        Bundle parametros= getIntent().getExtras();
        if(parametros!=null)
        {
            Contactos.strusuario=parametros.getString("Usu");
            Contactos.strcorreo=parametros.getString("Cor");
            Contactos.strtelefono=parametros.getString("tel");
            Contactos.agregar(1);
            Contactos.agregar(2);
            Contactos.agregar(3);
            list_usu.setAdapter(Recorre_list_usu);
        }
        list_usu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent pasCon_selec= new Intent(Area_ContactosActivity.this,Contacto_SeleccionadoActivity.class);
                pasCon_selec.putExtra("pos",position);
                startActivity(pasCon_selec);
                finish();
            }
        });
    }
}
