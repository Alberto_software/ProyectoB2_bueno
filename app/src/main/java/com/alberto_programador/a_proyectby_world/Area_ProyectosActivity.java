package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class Area_ProyectosActivity extends AppCompatActivity {

    private ImageButton bnproyecto,bnregre;
    private Intent intent1,regr;
    private ListView list_proyec;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area__proyectos);

        bnproyecto = (ImageButton) findViewById(R.id.nproy);
        bnregre = (ImageButton) findViewById(R.id.bregre);

        list_proyec = (ListView) findViewById(R.id.list_item_proyect);

        bnregre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regr= new Intent(Area_ProyectosActivity.this,MainActivity.class);
                startActivity(regr);
                finish();;
            }
        });

        ArrayAdapter<String> Recor_list_proy= new ArrayAdapter<String>
                (this,android.R.layout.simple_expandable_list_item_1,Proyectos.proyect);

        list_proyec.setAdapter(Recor_list_proy);

        bnproyecto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                intent1 = new Intent(Area_ProyectosActivity.this,Nuevo_ProyectoActivity.class);
                intent1.putExtra("validador",true);
                startActivity(intent1);
                finish();
            }
        });

        list_proyec.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent pasPro_selec= new Intent(Area_ProyectosActivity.this,Proyect_SeleccionadoActivity.class);
                pasPro_selec.putExtra("pos",position);
                startActivity(pasPro_selec);
                finish();
            }
        });
    }
}
