package com.alberto_programador.a_proyectby_world;

import java.util.ArrayList;

public class Proyectos {
    public static String strnombre,strdescripcion;
    public static Double strcosto;
    public static ArrayList<String> proyect= new ArrayList<String>();
    public static ArrayList<Double> cost= new ArrayList<Double>();
    public static ArrayList<String> Descrip= new ArrayList<String>();

    public static void agregar(int opc)
    {
        switch (opc)
        {
            case 1:
            {
                proyect.add(strnombre);
            }break;
            case 2:
            {
                cost.add(strcosto);
            }break;
            case 3:
            {
                Descrip.add(strdescripcion);
            }break;
        }
    }

    public static void borrar(int opc,int pos)
    {
        switch(opc)
        {
            case 1:
            {
                proyect.remove(pos);
            }break;
            case 2:
            {
                cost.remove(pos);
            }break;
            case 3:
            {
                Descrip.remove(pos);
            }break;
        }
    }

    public static void modificar(int opc, int pos)
    {
        switch (opc)
        {
            case 1:
            {
                proyect.remove(pos);
                proyect.add(pos,strnombre);
            }break;
            case 2:
            {
                cost.remove(strcosto);
                cost.add(pos,strcosto);
            }break;
            case 3:
            {
                Descrip.remove(pos);
                Descrip.add(pos,strdescripcion);
            }break;
        }
    }

}
