package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Nuevo_ContactosActivity extends AppCompatActivity {

    private EditText Entrada, Entrada2,Entrada3;
    private Button Click1,Click2;
    private String usuario,correo,telef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo__contactos);

        Entrada = (EditText) findViewById(R.id.get_usu);
        Entrada2 = (EditText) findViewById(R.id.get_corr);
        Entrada3= (EditText) findViewById(R.id.num_tel);

        Click1 = (Button) findViewById(R.id.Agre_Usu);
        Click2= (Button) findViewById(R.id.bcance);

        Bundle parametros= getIntent().getExtras();
        if((Boolean) parametros.get("Validador")!=true)
        {
            Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    usuario= Entrada.getText().toString();
                    correo= Entrada2.getText().toString();
                    telef= Entrada3.getText().toString();
                    Intent butVolver = new Intent(Nuevo_ContactosActivity.this, Area_ContactosActivity.class);
                    butVolver.putExtra("Usu",usuario);
                    butVolver.putExtra("Cor",correo);
                    butVolver.putExtra("tel",telef);
                    startActivity(butVolver);
                    finishActivity(R.layout.activity_nuevo__contactos);
                    finish();
                }
            });
            Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Cancelar= new Intent(Nuevo_ContactosActivity.this,Area_ContactosActivity.class);
                    startActivity(Cancelar);
                    finishActivity(R.layout.activity_nuevo__contactos);
                    finish();
                }
            });
        }
        else
        {
            final int pos=(Integer) parametros.get("pos");
            Click1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contactos.strusuario= Entrada.getText().toString();
                    Contactos.strcorreo= Entrada2.getText().toString();
                    Contactos.strtelefono= Entrada3.getText().toString();
                    Intent butVolver = new Intent(Nuevo_ContactosActivity.this, Area_ContactosActivity.class);
                    Contactos.modificar(1,pos);
                    Contactos.modificar(2,pos);
                    Contactos.modificar(3,pos);
                    startActivity(butVolver);
                    finishActivity(R.layout.activity_nuevo__contactos);
                    finish();
                }
            });
            Click2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Cancelar= new Intent(Nuevo_ContactosActivity.this,Area_ContactosActivity.class);
                    startActivity(Cancelar);
                    finishActivity(R.layout.activity_nuevo__contactos);
                    finish();
                }
            });
        }

    }
}