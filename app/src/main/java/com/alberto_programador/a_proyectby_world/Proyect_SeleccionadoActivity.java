package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Proyect_SeleccionadoActivity extends AppCompatActivity {

    ImageButton regre;
    ImageButton delet;
    TextView Title;
    TextView Cost;
    TextView Description;
    TextView btonPar;
    Button modif;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyect__seleccionado);

        regre= (ImageButton) findViewById(R.id.Reton2);
        delet= (ImageButton) findViewById(R.id.deletpro);

        Title= (TextView) findViewById(R.id.Titul_pro);
        Cost= (TextView) findViewById(R.id.let_cost);
        Description= (TextView) findViewById(R.id.let_descrip);
        btonPar= (TextView) findViewById(R.id.bpartici);

        modif=(Button) findViewById(R.id.bModi);

        Bundle parametros= getIntent().getExtras();
        pos=(Integer) parametros.get("pos");
        Proyectos.strcosto=Proyectos.cost.get(pos);
        Proyectos.strnombre=Proyectos.strcosto.toString();
        Title.setText(Proyectos.proyect.get(pos));
        Cost.setText(Proyectos.strnombre);
        Description.setText(Proyectos.Descrip.get(pos));

        regre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent reg= new Intent(Proyect_SeleccionadoActivity.this,Area_ProyectosActivity.class);
                startActivity(reg);
                finish();
            }
        });

        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Proyectos.borrar(1,pos);
                Proyectos.borrar(2,pos);
                Proyectos.borrar(3,pos);
                Intent reg2= new Intent(Proyect_SeleccionadoActivity.this,Area_ProyectosActivity.class);
                startActivity(reg2);
                finish();
            }
        });
        modif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent modif= new Intent(Proyect_SeleccionadoActivity.this,Nuevo_ProyectoActivity.class);
                modif.putExtra("validador",false);
                modif.putExtra("pos",pos);
                startActivity(modif);
                finish();
            }
        });
        btonPar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Guardado!",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
