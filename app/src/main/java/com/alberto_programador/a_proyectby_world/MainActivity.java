package com.alberto_programador.a_proyectby_world;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button M_butt_Contact;
    Button M_butt_Mis_Proyec;
    Button M_butt_Salir;
    Button M_butt_Proy_Inv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        M_butt_Contact= (Button) findViewById(R.id.Bcontact);
        M_butt_Mis_Proyec= (Button) findViewById(R.id.BMyProject);
        M_butt_Salir = (Button) findViewById(R.id.Bsalir);
        M_butt_Proy_Inv= (Button) findViewById(R.id.bPro_Inv);

        M_butt_Contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pasContact= new Intent(MainActivity.this,Area_ContactosActivity.class);
                startActivity(pasContact);
                finish();
            }
        });

        M_butt_Mis_Proyec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pasMisProy= new Intent(MainActivity.this,Area_ProyectosActivity.class);
                startActivity(pasMisProy);
                finish();
            }
        });

        M_butt_Salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

        M_butt_Proy_Inv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pasPro_Inv= new Intent(MainActivity.this,Aportacion_Activity.class);
                startActivity(pasPro_Inv);
                finish();
            }
        });
    }
}
