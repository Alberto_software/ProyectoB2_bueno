package com.alberto_programador.a_proyectby_world;

import android.content.Intent;
import android.icu.text.DecimalFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class Nuevo_ProyectoActivity extends AppCompatActivity {

    private EditText mnombre,mcosto,mdescripcion;
    private Intent intent2;
    private String costo;
    private Button baceptar,bcancelar;
    private CheckBox opc_todos,opc_perso;
    private int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo__proyecto);

        mnombre = (EditText) findViewById(R.id.nom_proyecto);
        mcosto = (EditText) findViewById(R.id.costo_tot);
        mdescripcion = (EditText) findViewById(R.id.desc_proyecto);

        opc_todos= (CheckBox) findViewById(R.id.checkBox_todos);
        opc_perso= (CheckBox) findViewById(R.id.checkBox2_perso);

        baceptar=(Button) findViewById(R.id.aceptar);
        bcancelar= (Button) findViewById(R.id.cancelar);
        Bundle parametros= getIntent().getExtras();
        if((Boolean) parametros.get("validador")==false) {
            pos= (Integer) parametros.get("pos");
            opc_todos.setEnabled(false);
            opc_perso.setEnabled(false);
            mnombre.setHint(Proyectos.proyect.get(pos));
            mcosto.setHint(Proyectos.cost.get(pos).toString());
            mdescripcion.setHint(Proyectos.Descrip.get(pos));
            baceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Proyectos.strnombre = mnombre.getText().toString();
                    costo = mcosto.getText().toString();
                    Proyectos.strcosto=Double.parseDouble(costo);
                    Proyectos.strdescripcion = mdescripcion.getText().toString();
                    Proyectos.modificar(1,pos);
                    Proyectos.modificar(2,pos);
                    Proyectos.modificar(3,pos);
                    intent2 = new Intent(Nuevo_ProyectoActivity.this, Area_ProyectosActivity.class);
                    startActivity(intent2);
                    finishActivity(R.layout.activity_nuevo__proyecto);
                    finish();
                }
            });
            bcancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent2 = new Intent(Nuevo_ProyectoActivity.this, Area_ProyectosActivity.class);
                    startActivity(intent2);
                    finishActivity(R.layout.activity_nuevo__proyecto);
                    finish();
                }
            });
        }
        else
        {
            opc_todos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(opc_todos.isChecked()==true)
                        opc_perso.setChecked(false);
                }
            });
            opc_perso.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(opc_perso.isChecked()==true)
                        opc_todos.setChecked(false);
                }
            });
            baceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Proyectos.strnombre = mnombre.getText().toString();
                    costo =mcosto.getText().toString();
                    Proyectos.strcosto=Double.parseDouble(costo);
                    Proyectos.strdescripcion = mdescripcion.getText().toString();
                    Proyectos.agregar(1);
                    Proyectos.agregar(2);
                    Proyectos.agregar(3);

                    if(opc_todos.isChecked()==true)
                    {
                        Participantes.opcion_todos();
                        intent2 = new Intent(Nuevo_ProyectoActivity.this, Area_ProyectosActivity.class);
                        startActivity(intent2);
                        finishActivity(R.layout.activity_nuevo__proyecto);
                        finish();
                    }
                    if(opc_perso.isChecked()==true)
                    {

                    }
                }
            });
            bcancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent2 = new Intent(Nuevo_ProyectoActivity.this, Area_ProyectosActivity.class);
                    startActivity(intent2);
                    finishActivity(R.layout.activity_nuevo__proyecto);
                    finish();
                }
            });
        }
    }
}
